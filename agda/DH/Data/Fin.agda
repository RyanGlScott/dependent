-- | Finite numbers.

module DH.Data.Fin where

open import DH.Prelude
open import DH.Data.Nat hiding ( cata )
open import DH.Data.Type.Nat as N

import DH.Data.Universe.Class as U

-------------------------------------------------------------------------------
-- Type
-------------------------------------------------------------------------------

-- | Finite numbers: @[0..n-1]@.
data Fin : Nat → Set where
    FZ : ∀ {n} → Fin (S n)
    FS : ∀ {n} → Fin n -> Fin (S n)

-------------------------------------------------------------------------------
-- Conversions
-------------------------------------------------------------------------------

-- | Fold 'Fin'.
cata : ∀ {n} → a → (a → a) → Fin n → a
cata {a = a} z f = go where
    go : ∀ {m} → Fin m → a
    go FZ = z
    go (FS n) = f (go n)

-- | Convert to 'Nat'.
toNat : ∀ {n} → Fin n → Nat
toNat = cata Z S

-- | Convert from 'Nat'.
--
-- >>> fromNat N.nat1 :: Maybe (Fin N.Nat2)
-- Just 1
--
-- >>> fromNat N.nat1 :: Maybe (Fin N.Nat1)
-- Nothing
--
fromNat : ∀ {n} → Nat -> Maybe (Fin n)
fromNat = N.induction (λ m → Nat → Maybe (Fin m)) start step where
    start : Nat → Maybe (Fin Z)
    start = const Nothing

    step : ∀ {n} → (Nat → Maybe (Fin n)) → Nat → Maybe (Fin (S n))
    step f Z = Just FZ
    step f (S m) = fmap FS (f m)


-- | Convert to 'ℕ'.
toℕ : Fin n -> ℕ
toℕ = cata 0 ℕ.suc

-- | Convert from any 'Ord' 'Num'.
unsafeFromNum : ∀ {n} {i : Set ℓ} → {{_ : Num i}} → {{_ : Ord i}} → i → Fin n
unsafeFromNum {n = n} {i = i} = N.induction (λ m → i → Fin m) start step
  where
    start : i → Fin Z
    start n with compare n 0
    ... | LT = throw Underflow
    ... | EQ = throw Overflow
    ... | GT = throw Overflow

    step : ∀ {m} → (i → Fin m) → i → Fin (S m)
    step f n with compare n 0
    ... | EQ = FZ
    ... | GT = FS (f (n - 1))
    ... | LT = throw Underflow

-------------------------------------------------------------------------------
-- Instances
-------------------------------------------------------------------------------

eqFin : ∀ {n} → Fin n → Fin n → Bool
eqFin FZ FZ = true
eqFin FZ (FS f2) = false
eqFin (FS f1) FZ = false
eqFin (FS f1) (FS f2) = eqFin f1 f2

cmpFin : ∀ {n} → Fin n → Fin n → Ordering
cmpFin FZ FZ = EQ
cmpFin FZ (FS f2) = LT
cmpFin (FS f1) FZ = GT
cmpFin (FS f1) (FS f2) = cmpFin f1 f2

instance
  EqFin : ∀ {n} → Eq (Fin n)
  EqFin = record { _==_ = eqFin }

instance
  OrdFin : ∀ {n} → Ord (Fin n)
  OrdFin = record { compare = cmpFin }

fromEnumFin : ∀ {n} → Fin n → ℤ
fromEnumFin FZ = 0
fromEnumFin (FS f) = succ (fromEnumFin f)

instance
  EnumFin : Enum (Fin n)
  EnumFin = record { fromEnum = fromEnumFin; toEnum = unsafeFromNum }

signumFin : Fin n → Fin n
signumFin FZ = FZ
signumFin (FS FZ) = FS FZ
signumFin (FS (FS f)) = FS FZ

toℤFin : Fin n → ℤ
toℤFin = cata 0 succ

fromℤFin : ℤ → Fin n
fromℤFin {n} x = unsafeFromNum (mod (fromℤ x) n)

-- | Operations module @n@.
--
-- >>> map fromInteger [0, 1, 2, 3, 4, -5] :: [Fin N.Nat3]
-- [0,1,2,0,1,1]
--
-- >>> fromInteger 42 :: Fin N.Nat0
-- *** Exception: divide by zero
-- ...
--
-- >>> signum (FZ :: Fin N.Nat1)
-- 0
--
-- >>> signum (3 :: Fin N.Nat4)
-- 1
--
-- >>> 2 + 3 :: Fin N.Nat4
-- 1
--
-- >>> 2 * 3 :: Fin N.Nat4
-- 2
--
instance
  NumFin : ∀ {n : Nat} → Num (Fin n)
  NumFin {n} = record { _+_ = λ x y → fromℤFin (toℤFin x + toℤFin y)
                      ; _-_ = λ x y → fromℤFin (toℤFin x - toℤFin y)
                      ; _*_ = λ x y → fromℤFin (toℤFin x * toℤFin y)
                      ; negate = λ x → fromℤFin (negate (toℤFin x))
                      ; abs = λ x → x
                      ; signum = signumFin
                      ; fromℕ = λ x → unsafeFromNum (mod (fromℕ x) n) }

-- | Multiplicative inverse.
--
-- Works for @'Fin' n@ where @n@ is coprime with an argument, i.e. in general when @n@ is prime.
--
-- >>> map inverse universe :: [Fin N.Nat5]
-- [0,1,3,2,4]
--
-- >>> zipWith (*) universe (map inverse universe) :: [Fin N.Nat5]
-- [0,1,1,1,1]
--
-- Adaptation of [pseudo-code in Wikipedia](https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers)
--
{-# TERMINATING #-}
inverse : ∀ {n} → Fin n → Fin n
inverse {n} = fromℤFin ∘ iter 0 (toℤ n) 1 ∘ toℤFin where
    iter : ℤ → ℤ → ℤ → ℤ → ℤ
    iter t _ _ (+_ ℕ.zero) with t < 0
    ... | true  = t + toℤ n
    ... | false = t
    iter t r t' r' =
        let q = div r r'
        in iter t' r' (t - q * t') (r - q * r')

-- | 'quot' works only on @'Fin' n@ where @n@ is prime.
quotFin : Fin n → Fin n → Fin n
quotFin a b = a * inverse b

quotRemFin : Fin n → Fin n → Fin n × Fin n
quotRemFin a b = quotFin a b , 0

instance
  IntegralFin : Integral (Fin n)
  IntegralFin = record { quotRem = quotRemFin
                       ; toℤ = toℤFin
                       ; fromℤ = fromℤFin }

-------------------------------------------------------------------------------
-- "Interesting" stuff
-------------------------------------------------------------------------------

-- | All values. @[minBound .. maxBound]@ won't work for @'Fin' 'N.Nat0'@.
--
-- >>> universe :: [Fin N.Nat3]
-- [0,1,2]
universe : ∀ {n} → List (Fin n)
universe = N.induction (λ m → List (Fin m)) [] step where
    step : ∀ {n} → List (Fin n) → List (Fin (S n))
    step xs = FZ ∷ map FS xs

-- | Like 'universe' but 'NonEmpty'.
--
-- >>> universe1 :: NonEmpty (Fin N.Nat3)
-- 0 :| [1,2]
universe1 : ∀ {n} → NonEmpty (Fin (S n))
universe1 = N.induction (λ m → NonEmpty (Fin (S m))) (FZ :| []) step where
    step : ∀ {n} → NonEmpty (Fin (S n)) → NonEmpty (Fin (S (S n)))
    step xs = cons FZ (fmap FS xs)

-- | 'universe' which will be fully inlined, if @n@ is known at compile time.
--
-- >>> inlineUniverse :: [Fin N.Nat3]
-- [0,1,2]
inlineUniverse : ∀ {n} → {{_ : N.InlineInduction n}} → List (Fin n)
inlineUniverse = N.inlineInduction (λ m → List (Fin m)) [] step where
    step : ∀ {n} → List (Fin n) → List (Fin (S n))
    step xs = FZ ∷ map FS xs

-- | >>> inlineUniverse1 :: NonEmpty (Fin N.Nat3)
-- 0 :| [1,2]
inlineUniverse1 : ∀ {n} → {{_ : N.InlineInduction n}} → NonEmpty (Fin (S n))
inlineUniverse1 = N.inlineInduction (λ m → NonEmpty (Fin (S m))) (FZ :| []) step where
    step : ∀ {n} → NonEmpty (Fin (S n)) -> NonEmpty (Fin (S (S n)))
    step xs = cons FZ (fmap FS xs)

-- | @'Fin' 'N.Nat0'@ is not inhabited.
absurd : ∀ {ℓ} {b : Set ℓ} → Fin 0 → b
absurd ()

-- | Counting to one is boring.
--
-- >>> boring
-- 0
boring : Fin 1
boring = FZ

-------------------------------------------------------------------------------
-- Append & Split
-------------------------------------------------------------------------------

-- | >>> map weakenRight1 universe :: [Fin N.Nat5]
-- [1,2,3,4]
--
-- @since 0.1.1
weakenRight1 : ∀ {n} → Fin n → Fin (S n)
weakenRight1 = FS

-- | >>> map weakenLeft1 universe :: [Fin N.Nat5]
-- [0,1,2,3]
--
-- @since 0.1.1
weakenLeft1 : ∀ {n} → {{_ : N.InlineInduction n}} → Fin n → Fin (S n)
weakenLeft1 = N.inlineInduction (λ m → Fin m → Fin (S m)) start step where
    start : Fin Z → Fin (S Z)
    start = absurd

    step : ∀ {n} → (Fin n → Fin (S n)) → Fin (S n) → Fin (S (S n))
    step go FZ = FZ
    step go (FS n') = FS (go n')

-- | >>> map (weakenLeft (Proxy :: Proxy N.Nat2)) (universe :: [Fin N.Nat3])
-- [0,1,2]
weakenLeft : ∀ {n} {m} → {{_ : N.InlineInduction n}} → Fin n → Fin (n + m)
weakenLeft {m = m} = N.inlineInduction (λ p → Fin p → Fin (p + m)) start step where
    start : Fin Z → Fin (Z + m)
    start = absurd

    step : ∀ {p} → (Fin p → Fin (p + m)) → Fin (S p) → Fin (S p + m)
    step go FZ = FZ
    step go (FS n') = FS (go n')

-- | >>> map (weakenRight (Proxy :: Proxy N.Nat2)) (universe :: [Fin N.Nat3])
-- [2,3,4]
weakenRight : ∀ {n} {m} → {{_ : N.InlineInduction n}} → Fin m → Fin (n + m)
weakenRight {m = m} = N.inlineInduction WeakenRight start step where
    WeakenRight : Nat → Set
    WeakenRight q = Fin m → Fin (q + m)

    start : WeakenRight Z
    start = id

    step : ∀ {p} → WeakenRight p → WeakenRight (S p)
    step go x = FS $ go x

-- | Append two 'Fin's together.
--
-- >>> append (Left fin2 :: Either (Fin N.Nat5) (Fin N.Nat4))
-- 2
--
-- >>> append (Right fin2 :: Either (Fin N.Nat5) (Fin N.Nat4))
-- 7
--
append : ∀ {n} {m} → {{_ : N.InlineInduction n}} → Either (Fin n) (Fin m) → Fin (n + m)
append {m = m} (Left n)  = weakenLeft {m = m} n
append {n = n} (Right m) = weakenRight {n = n} m

-- | Inverse of 'append'.
--
-- >>> split fin2 :: Either (Fin N.Nat2) (Fin N.Nat3)
-- Right 0
--
-- >>> split fin1 :: Either (Fin N.Nat2) (Fin N.Nat3)
-- Left 1
--
-- >>> map split universe :: [Either (Fin N.Nat2) (Fin N.Nat3)]
-- [Left 0,Left 1,Right 0,Right 1,Right 2]
--
split : ∀ {n} {m} → {{_ : N.InlineInduction n}} → Fin (n + m) -> Either (Fin n) (Fin m)
split {m = m} = N.inlineInduction Split start step where
    Split : Nat → Set
    Split p = Fin (p + m) → Either (Fin p) (Fin m)

    start : Split Z
    start = Right

    step : ∀ {p} → Split p → Split (S p)
    step go FZ = Left FZ
    step go (FS x') = bimap FS id $ go x'

-- | Mirror the values, 'minBound' becomes 'maxBound', etc.
--
-- >>> map mirror universe :: [Fin N.Nat4]
-- [3,2,1,0]
--
-- >>> reverse universe :: [Fin N.Nat4]
-- [3,2,1,0]
--
-- @since 0.1.1
--
mirror : ∀ {n} → {{_ : N.InlineInduction n}} → Fin n → Fin n
mirror = N.inlineInduction (λ m → Fin m → Fin m) start step where
    start : Fin Z → Fin Z
    start = id

    step : ∀ {m} → {{_ : N.InlineInduction m}} → (Fin m → Fin m) → Fin (S m) → Fin (S m)
    step rec FZ = N.inlineInduction (λ p → Fin (S p)) FZ FS
    step rec (FS m) = weakenLeft1 (rec m)

maxBoundFin : Fin (S n)
maxBoundFin = N.induction (λ m → Fin (S m)) FZ FS

instance
  BoundedFin : ∀ {n} {m} → {pf : n ≡ S m} → Bounded (Fin n)
  BoundedFin {pf = pf} rewrite pf = record { minBound = FZ
                                           ; maxBound = maxBoundFin }
                                           
-------------------------------------------------------------------------------
-- universe-base
-------------------------------------------------------------------------------

-- | @since 0.1.2
instance
  UniverseFin : U.Universe (Fin n)
  UniverseFin = record { universe = universe }

-- | 
--
-- >>> (U.cardinality :: U.Tagged (Fin N.Nat3) Natural) == U.Tagged (genericLength (U.universeF :: [Fin N.Nat3]))
-- True
--
-- @since 0.1.2
--
instance
  FiniteFin : U.Finite (Fin n)
  FiniteFin = record { universeF = U.universe }

-------------------------------------------------------------------------------
-- min and max
-------------------------------------------------------------------------------

-- | Return a one less.
--
-- >>> isMin (FZ :: Fin N.Nat1)
-- Nothing
--
-- >>> map isMin universe :: [Maybe (Fin N.Nat3)]
-- [Nothing,Just 0,Just 1,Just 2]
--
-- @since 0.1.1
--
isMin : Fin (S n) → Maybe (Fin n)
isMin FZ     = Nothing
isMin (FS n) = Just n

-- | Return a one less.
--
-- >>> isMax (FZ :: Fin N.Nat1)
-- Nothing
--
-- >>> map isMax universe :: [Maybe (Fin N.Nat3)]
-- [Just 0,Just 1,Just 2,Nothing]
--
-- @since 0.1.1
--
isMax : ∀ {n} → {{_ : N.InlineInduction n}} → Fin (S n) → Maybe (Fin n)
isMax = N.inlineInduction IsMax start step where
    IsMax : Nat → Set
    IsMax n = Fin (S n) → Maybe (Fin n)

    start : IsMax Z
    start _ = Nothing

    step : ∀ {m} → IsMax m → IsMax (S m)
    step rec FZ = Just FZ
    step rec (FS m) = fmap FS (rec m)

-------------------------------------------------------------------------------
-- Aliases
-------------------------------------------------------------------------------

fin0 : Fin (0 + S n)
fin1 : Fin (1 + S n)
fin2 : Fin (2 + S n)
fin3 : Fin (3 + S n)
fin4 : Fin (4 + S n)
fin5 : Fin (5 + S n)
fin6 : Fin (6 + S n)
fin7 : Fin (7 + S n)
fin8 : Fin (8 + S n)
fin9 : Fin (9 + S n)

fin0 = FZ
fin1 = FS fin0
fin2 = FS fin1
fin3 = FS fin2
fin4 = FS fin3
fin5 = FS fin4
fin6 = FS fin5
fin7 = FS fin6
fin8 = FS fin7
fin9 = FS fin8

