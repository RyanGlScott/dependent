-- | 'Nat' numbers. @DataKinds@ stuff.

module DH.Data.Type.Nat where

open import DH.Prelude
open import DH.Data.Type.Dec
open import DH.Data.Nat using ( Nat; Z; S; n )

-------------------------------------------------------------------------------
-- Induction
-------------------------------------------------------------------------------

-- | Induction on 'Nat'.
--
-- Useful in proofs or with GADTs, see source of 'proofPlusNZero'.
induction
    : ∀ {n : Nat}
    → (t : Nat → Set ℓ)
    → t Z                       -- ^ zero case
    → (∀ {m} → t m -> t (S m))  -- ^ induction step
    → t n
induction {n = n} t z f = go
  where
    go : ∀ {m} → t m
    go {m = Z} = z
    go {m = S _} = f go

-- | Induction on 'Nat', functor form. Useful for computation.
--
-- >>> induction1 (Tagged 0) $ retagMap (+2) :: Tagged Nat3 Int
-- Tagged 6
--
induction1
    : ∀ {n : Nat} {a : Set ℓ}
    → (t : Nat → Set ℓ → Set ℓ')
    → t Z a                        -- ^ zero case
    → (∀ {m} → t m a → t (S m) a)  -- ^ induction step
    → t n a
induction1 {a = a} t = induction (λ m → t m a)

-- | The induction will be fully inlined.
{-# NO_POSITIVITY_CHECK #-}
record InlineInduction (n : Nat) : Setω where
  coinductive
  field
    inlineInduction : (t : Nat → Set ℓ)
                    → t Z
                    → (∀ {m} → {{_ : InlineInduction m}} → t m → t (S m))
                    → t n

open InlineInduction {{...}} public

instance
  InlineInductionZ : InlineInduction Z
  InlineInductionZ = record { inlineInduction = λ _ z _ → z }

inlineInductionS : {{_ : InlineInduction n}}
                  → (t : Nat → Set ℓ)
                  → t Z
                  → (∀ {m} → {{_ : InlineInduction m}} → t m → t (S m))
                  → t (S n)
inlineInductionS t z f = f (inlineInduction t z f)

instance
  InlineInductionS : {{_ : InlineInduction n}} → InlineInduction (S n)
  InlineInductionS = record { inlineInduction = λ {ℓ} t → inlineInductionS t }

-- | Unfold @n@ steps of a general recursion.
--
-- /Note:/ Always __benchmark__. This function may give you both /bad/ properties:
-- a lot of code (increased binary size), and worse performance.
--
-- For known @n@ 'unfoldedFix' will unfold recursion, for example
--
-- @
-- 'unfoldedFix' ('Proxy' :: 'Proxy' 'Nat3') f = f (f (f (fix f)))
-- @
--
unfoldedFix : ∀ n {a : Set ℓ} → {{_ : InlineInduction n}} → (a → a) → a
unfoldedFix n {a = a} = inlineInduction {n = n} (λ m → (a → a) → a) start step where
    start : (a → a) → a
    start = fix

    step : ((a → a) → a) → (a → a) → a
    step go f = f (go f)

-------------------------------------------------------------------------------
-- Equality
-------------------------------------------------------------------------------

-- | Decide equality of type-level numbers.
--
-- >>> eqNat :: Maybe (Nat3 :~: Plus Nat1 Nat2)
-- Just Refl
--
-- >>> eqNat :: Maybe (Nat3 :~: Mult Nat2 Nat2)
-- Nothing
--
eqNat : ∀ {n} {m} → Maybe (n ≡ m)
eqNat = induction (λ p → ∀ {q} → Maybe (p ≡ q)) start step where
    start : ∀ {p} → Maybe (Z ≡ p)
    start {p = Z} = Just refl
    start {p = S _} = Nothing

    step' : ∀ {p} {q} → (∀ {q'} → Maybe (p ≡ q')) -> Maybe (S p ≡ S q)
    step' {q = q} hind with hind {q' = q}
    ... | Nothing = Nothing
    ... | Just refl = Just refl

    step : ∀ {p} → (∀ {q'} → Maybe (p ≡ q')) -> ∀ {q} → Maybe (S p ≡ q)
    step hind {q = Z} = Nothing
    step hind {q = S _} = step' hind

-- | Decide equality of type-level numbers.
--
-- >>> decShow (discreteNat :: Dec (Nat3 :~: Plus Nat1 Nat2))
-- "Yes Refl"
--
-- @since 0.0.3
discreteNat : ∀ {n} {m} → Dec (n ≡ m)
discreteNat = induction (λ p → ∀ {q} → Dec (p ≡ q)) start step
  where
    start : ∀ {p} → Dec (Z ≡ p)
    start {p = Z} = Yes refl
    start {p = S _} = No $ λ ()

    unS : ∀ {p} {q} → S p ≡ S q → p ≡ q
    unS refl = refl

    step' : ∀ {p} → (∀ {q} → Dec (p ≡ q)) → ∀ {q} → Dec (S p ≡ S q)
    step' rec {q = q} with rec {q = q}
    ... | Yes refl = Yes refl
    ... | No np = No $ λ snp → np (unS snp)

    step : ∀ {p} → (∀ {q} → Dec (p ≡ q)) → ∀ {q} → Dec (S p ≡ q)
    step rec {q = Z} = No $ λ ()
    step rec {q = S _} = step' rec

instance
  EqDNat : EqD Nat
  EqDNat = record { _===_ = λ p q → discreteNat }

-------------------------------------------------------------------------------
-- Arithmetic
-------------------------------------------------------------------------------

-- | Multiplication by two. Doubling.
--
-- >>> reflect (snat :: SNat (Mult2 Nat4))
-- 8
--
mult2 : Nat → Nat
mult2 Z = Z
mult2 (S n) = S (S (mult2 n))

-- | Division by two. 'False' is 0 and 'True' is 1 as a remainder.
--
-- >>> :kind! DivMod2 Nat7
-- DivMod2 Nat7 :: (Nat, Bool)
-- = '( 'S ('S ('S 'Z)), 'True)
--
-- >>> :kind! DivMod2 Nat4
-- DivMod2 Nat4 :: (Nat, Bool)
-- = '( 'S ('S 'Z), 'False)
--
divMod2 : Nat → (Nat × Bool)
divMod2 Z = Z , false
divMod2 (S Z) = Z , true
divMod2 (S (S n)) with divMod2 n
... | n' , b = S n' , b

-------------------------------------------------------------------------------
-- proofs
-------------------------------------------------------------------------------

-- | @0 + n = n@
proofPlusZeroN : 0 + n ≡ n
proofPlusZeroN = refl

-- | @n + 0 = n@
proofPlusNZero : n + 0 ≡ n
proofPlusNZero = induction (λ m → m + 0 ≡ m) refl step where
    step : ∀ {m} → m + 0 ≡ m → S m + 0 ≡ S m
    step pf rewrite pf = refl

-- | @n + 1 = S n
proofPlusNOne : n + 1 ≡ S n
proofPlusNOne = induction (λ m → m + 1 ≡ S m) refl step where
  step : ∀ {m} → m + 1 ≡ S m → S m + 1 ≡ S (S m)
  step pf rewrite pf = refl

-- | @0 * n = 0@
proofMultZeroN : 0 * n ≡ 0
proofMultZeroN = refl

-- | @n * 0 = 0@
proofMultNZero : ∀ (n : Nat) → n * 0 ≡ 0
proofMultNZero n = induction {n = n} (λ m → m * 0 ≡ 0) refl (λ {m} → step m)
  where
    step : ∀ (m : Nat) → m * 0 ≡ 0 -> S m * 0 ≡ 0
    step _ pf rewrite pf = refl

-- | @1 * n = n@
proofMultOneN : 1 * n ≡ n
proofMultOneN {n} rewrite proofPlusNZero {n} = refl

-- | @n * 1 = n@
proofMultNOne : n * 1 ≡ n
proofMultNOne = induction (λ m → m * 1 ≡ m) refl step where
    step : ∀ {m} → m * 1 ≡ m -> S m * 1 ≡ S m
    step {m} pf rewrite pf | proofPlusNOne {m} = refl
