module DH.Prelude where

open import Level public
import Data.Bool
open Data.Bool using ( Bool; false; true; not; if_then_else_ ) public
import Relation.Binary.PropositionalEquality
open Relation.Binary.PropositionalEquality using ( _≡_; refl ) public
open Relation.Binary.PropositionalEquality using ( inspect; [_] )
open import Data.Product using ( _×_; _,_ ) renaming ( proj₁ to fst;
                                                       proj₂ to snd ) public
open import Data.Empty using ( ⊥ ) public

import Data.Nat
open Data.Nat using ( ℕ ) public
import Data.Nat.DivMod

import Data.Integer
open Data.Integer using ( ℤ; +_ ) public
open Data.Integer using ( ∣_∣ )
open import Data.Integer.DivMod renaming ( _div_ to _divℤ_; _mod_ to _modℤ_ )

open import Data.List using ( List; []; _∷_; map; concat; _++_ ) public

open import Relation.Nullary using ( Dec; _because_; yes; no )

open import Data.Unit public

variable
  ℓ ℓ' ℓ₀ ℓ₁ ℓ₂ : Level
  a b c : Set ℓ

postulate
  admit : a    -- help me to remove usages of this

_&&_ _||_ : Bool → Bool → Bool
_&&_ = Data.Bool._∧_
_||_ = Data.Bool._∨_

infix 2 _&&_
infix 1 _||_

--------------------------------------------------------------------------------
-- Utility functions

_$_ : (a → b) → a → b
f $ x = f x
infixr 9 _$_

const : ∀ {a : Set ℓ} {b : Set ℓ'} → a → b → a
const x _ = x

{-# NON_TERMINATING #-}
fix : (a → a) → a
fix f = f (fix f)

_∘_ : (b → c) → (a → b) → a → c
(f ∘ g) x = f (g x)
infixr 9 _∘_

id : a → a
id x = x

uncurry : (a → b → c) → (a × b) → c
uncurry f (x , y) = f x y

--------------------------------------------------------------------------------
-- Exceptions

postulate
  Exception : Set ℓ → Set ℓ

postulate
  throw : {{_ : Exception a}} → a → b

data ArithException : Set where
  Overflow Underflow DivideByZero : ArithException

data TypeCheckingException : Set where
  StuckTypeFamily : TypeCheckingException -- used to label missing type family instances

instance postulate ExceptionArithException : Exception ArithException
instance postulate ExceptionTypeCheckingException : Exception TypeCheckingException

--------------------------------------------------------------------------------
-- The Eq type class

record Eq (a : Set ℓ) : Set ℓ where
  field
    _==_ : a → a → Bool
  infix 3 _==_

open Eq {{...}} public

_/=_ : {{_ : Eq a}} → a → a → Bool
x /= y = not (x == y)
infix 3 _/=_

instance
  Eqℕ : Eq ℕ
  Eqℕ = record { _==_ = Data.Nat._≡ᵇ_ }

eqBool : Bool → Bool → Bool
eqBool false false = true
eqBool false true = false
eqBool true false = false
eqBool true true = true

instance
  EqBool : Eq Bool
  EqBool = record { _==_ = eqBool }

eqℤ : ℤ → ℤ → Bool
eqℤ (ℤ.pos n) (ℤ.pos n₁) = n == n₁
eqℤ (ℤ.pos n) (ℤ.negsuc n₁) = false
eqℤ (ℤ.negsuc n) (ℤ.pos n₁) = false
eqℤ (ℤ.negsuc n) (ℤ.negsuc n₁) = n == n₁

instance
  Eqℤ : Eq ℤ
  Eqℤ = record { _==_ = eqℤ }

-------------------------------------------------------------------------------
-- The Ord type class

data Ordering : Set where
  LT EQ GT : Ordering

record Ord (a : Set ℓ) : Set ℓ where
  field
    {{EqA}} : Eq a
    compare : a → a → Ordering

open Ord {{...}} public

cmpℕ : ℕ → ℕ → Ordering
cmpℕ ℕ.zero ℕ.zero = EQ
cmpℕ ℕ.zero (ℕ.suc n2) = LT
cmpℕ (ℕ.suc n1) ℕ.zero = GT
cmpℕ (ℕ.suc n1) (ℕ.suc n2) = cmpℕ n1 n2

instance
  Ordℕ : Ord ℕ
  Ordℕ = record { compare = cmpℕ }

cmpBool : Bool → Bool → Ordering
cmpBool false false = EQ
cmpBool false true = LT
cmpBool true false = GT
cmpBool true true = EQ

instance
  OrdBool : Ord Bool
  OrdBool = record { compare = cmpBool }

cmpℤ : ℤ → ℤ → Ordering
cmpℤ (ℤ.pos n) (ℤ.pos n₁) = compare n n₁
cmpℤ (ℤ.pos n) (ℤ.negsuc n₁) = GT
cmpℤ (ℤ.negsuc n) (ℤ.pos n₁) = LT
cmpℤ (ℤ.negsuc n) (ℤ.negsuc n₁) = compare n₁ n

instance
  Ordℤ : Ord ℤ
  Ordℤ = record { compare = cmpℤ }

_<_ : {{_ : Ord a}} → a → a → Bool
x < y with compare x y
... | LT = true
... | _  = false

_<=_ : {{_ : Ord a}} → a → a → Bool
x <= y with compare x y
... | GT = false
... | _  = true

_>_ : {{_ : Ord a}} → a → a → Bool
x > y with compare x y
... | GT = true
... | _  = false

_>=_ : {{_ : Ord a}} → a → a → Bool
x >= y with compare x y
... | LT = false
... | _  = true

infix 4 _<_ _<=_ _>_ _>=_

-------------------------------------------------------------------------------
-- The Num type class

record Num (a : Set ℓ) : Set ℓ where
  field
    _+_ _-_ _*_ : a → a → a
    negate abs signum : a → a
    fromℕ : ℕ → a

  infixl 6 _+_ _-_
  infixl 7 _*_

open Num {{...}} public

{-# BUILTIN FROMNAT fromℕ #-}

signumℕ : ℕ → ℕ
signumℕ ℕ.zero = ℕ.zero
signumℕ (ℕ.suc n) = ℕ.suc ℕ.zero

instance
  Numℕ : Num ℕ
  Numℕ = record { _+_ = Data.Nat._+_
                ; _-_ = Data.Nat._∸_
                ; _*_ = Data.Nat._*_
                ; negate = λ _ → ℕ.zero
                ; abs = λ n → n
                ; signum = signumℕ
                ; fromℕ = λ n → n }

signumℤ : ℤ → ℤ
signumℤ (ℤ.pos ℕ.zero) = Data.Integer.0ℤ
signumℤ (ℤ.pos (ℕ.suc _)) = Data.Integer.1ℤ
signumℤ (ℤ.negsuc n) = Data.Integer.-1ℤ

instance
  Numℤ : Num ℤ
  Numℤ = record { _+_ = Data.Integer._+_
                ; _-_ = Data.Integer._-_
                ; _*_ = Data.Integer._*_
                ; negate = Data.Integer.-_
                ; abs = λ z → Data.Integer.+_ (Data.Integer.∣_∣ z)
                ; signum = signumℤ
                ; fromℕ = Data.Integer.+_ }

------------------------------------------------------------------------------
-- The Integral type class

record Integral (a : Set ℓ) : Set ℓ where
  field
    {{NumA}} : Num a
    {{OrdA}} : Ord a
    quotRem : a → a → a × a
    fromℤ : ℤ → a
    toℤ : a → ℤ

open Integral {{...}} public

divMod : {{_ : Integral a}} → a → a → a × a
divMod {a = a} n d = if signum r == negate (signum d) then (q - 1 , r + d) else qr
  where
    qr : a × a
    qr = quotRem n d

    q r : a
    q = fst qr
    r = snd qr

quot : {{_ : Integral a}} → a → a → a
quot x y = fst (quotRem x y)

rem : {{_ : Integral a}} → a → a → a
rem x y = snd (quotRem x y)

div : {{_ : Integral a}} → a → a → a
div x y = fst (divMod x y)

mod : {{_ : Integral a}} → a → a → a
mod x y = snd (divMod x y)

quotRemℕ : ℕ → ℕ → ℕ × ℕ
quotRemℕ n1 n2 = Data.Nat.DivMod._/_ n1 n2 {admit} , Data.Nat.DivMod._%_ n1 n2 {admit}

fromℤℕ : ℤ → ℕ
fromℤℕ (+_ n) = n
fromℤℕ (ℤ.negsuc n) = throw Underflow

instance
  Integralℕ : Integral ℕ
  Integralℕ = record { quotRem = quotRemℕ
                     ; fromℤ = fromℤℕ
                     ; toℤ = Data.Integer.+_ }

divMod→quotRem : {{_ : Ord a}} → {{_ : Num a}} → (a → a → a × a) → a → a → a × a
divMod→quotRem dm num denom with dm num denom
... | div , mod = if (num < 0 /= denom < 0) && (mod /= 0)
                  then div + 1 , mod - denom else div , mod

divModℤ : ℤ → ℤ → ℤ × ℤ
divModℤ x y with Data.Nat._≟_ ∣ y ∣ 0
divModℤ x y | no ¬p = (x divℤ y) {admit} , + ((x modℤ y) {admit})
  -- I would like to get of those `admit`s, but I don't know enough Agda.
divModℤ x y | yes p = throw DivideByZero

quotRemℤ : ℤ → ℤ → ℤ × ℤ
quotRemℤ = divMod→quotRem divModℤ

instance
  Integralℤ : Integral ℤ
  Integralℤ = record { quotRem = quotRemℤ
                     ; fromℤ = λ z → z
                     ; toℤ = λ z → z }


------------------------------------------------------------------------------
-- The Enum type class

record Enum (a : Set ℓ) : Set ℓ where
  field
    toEnum : ℤ → a
    fromEnum : a → ℤ

open Enum {{...}} public

succ : {{_ : Enum a}} → a → a
succ x = toEnum (1 + (fromEnum x))

enumFromToℤ : ℤ → ℤ → List ℤ
enumFromToℤ lo hi = map (λ n → lo + n) (Data.List.reverse (upto (hi - lo)))
  where
    uptoℕ : ℕ → List ℕ
    uptoℕ ℕ.zero = ℕ.zero ∷ []
    uptoℕ (ℕ.suc n) = ℕ.suc n ∷ uptoℕ n

    upto : ℤ → List ℤ
    upto (+_ n) = map +_ (uptoℕ n)
    upto (ℤ.negsuc n) = []

enumFromTo : {{_ : Enum a}} → a → a → List a
enumFromTo lo hi = map toEnum (enumFromToℤ (fromEnum lo) (fromEnum hi))

toEnumℕ : ℤ → ℕ
toEnumℕ (+_ n) = n
toEnumℕ (ℤ.negsuc n) = throw Underflow

fromEnumℕ : ℕ → ℤ
fromEnumℕ = Data.Integer.+_

instance
  Enumℕ : Enum ℕ
  Enumℕ = record { toEnum = toEnumℕ
                 ; fromEnum = fromEnumℕ }

instance
  Enumℤ : Enum ℤ
  Enumℤ = record { toEnum = λ z → z 
                 ; fromEnum = λ z → z }

toEnumBool : ℤ → Bool
toEnumBool (+_ ℕ.zero) = false
toEnumBool Data.Integer.+[1+ ℕ.zero ] = true
toEnumBool Data.Integer.+[1+ ℕ.suc n ] = throw Overflow
toEnumBool (ℤ.negsuc n) = throw Underflow

fromEnumBool : Bool → ℤ
fromEnumBool false = 0
fromEnumBool true = 1

instance
  EnumBool : Enum Bool
  EnumBool = record { toEnum = toEnumBool
                    ; fromEnum = fromEnumBool }

------------------------------------------------------------------------------
-- The Bounded type class

record Bounded (a : Set ℓ) : Set ℓ where
  field
    minBound maxBound : a

open Bounded {{...}} public

instance
  BoundedBool : Bounded Bool
  BoundedBool = record { minBound = false
                       ; maxBound = true }

------------------------------------------------------------------------------
-- The Functor type class

record Functor (f : Set ℓ → Set ℓ) : Set (suc ℓ) where
  field
    fmap : ∀ {a b : Set ℓ} → (a → b) → (f a → f b)

open Functor {{...}} public

instance
  FunctorList : ∀ {ℓ} → Functor {ℓ = ℓ} List
  FunctorList = record { fmap = map }

------------------------------------------------------------------------------
-- The Bifunctor type class

record Bifunctor (p : Set ℓ → Set ℓ → Set ℓ) : Set (suc ℓ) where
  field
    bimap : ∀ {a b c d : Set ℓ} → (a → b) → (c → d) → p a c → p b d

open Bifunctor {{...}} public

first : ∀ {p : ∀ {ℓ} → Set ℓ → Set ℓ → Set ℓ} {a b c : Set ℓ} → {{_ : Bifunctor p}}
      → (a → c) → p a b → p c b
first f = bimap f id

second : ∀ {p : ∀ {ℓ} → Set ℓ → Set ℓ → Set ℓ} {a b c : Set ℓ} → {{_ : Bifunctor p}}
       → (b → c) → p a b → p a c
second = bimap id

---------------------------------------------------------------------------------
-- The Maybe type

data Maybe (a : Set ℓ) : Set ℓ where
  Nothing : Maybe a
  Just : a → Maybe a

eqMaybe : {{_ : Eq a}} → Maybe a → Maybe a → Bool
eqMaybe Nothing Nothing = true
eqMaybe Nothing (Just x) = false
eqMaybe (Just x) Nothing = false
eqMaybe (Just x) (Just x₁) = x == x₁

instance
  EqMaybe : {{_ : Eq a}} → Eq (Maybe a)
  EqMaybe = record { _==_ = eqMaybe }

cmpMaybe : {{_ : Ord a}} → Maybe a → Maybe a → Ordering
cmpMaybe Nothing Nothing = EQ
cmpMaybe Nothing (Just x) = LT
cmpMaybe (Just x) Nothing = GT
cmpMaybe (Just x) (Just x₁) = compare x x₁

instance
  OrdMaybe : {{_ : Ord a}} → Ord (Maybe a)
  OrdMaybe = record { compare = cmpMaybe }

fmapMaybe : ∀ {a b : Set ℓ} → (a → b) → Maybe a → Maybe b
fmapMaybe f Nothing = Nothing
fmapMaybe f (Just x) = Just (f x)

instance
  FunctorMaybe : ∀ {ℓ} → Functor {ℓ = ℓ} Maybe
  FunctorMaybe = record { fmap = fmapMaybe }

---------------------------------------------------------------------------------
-- The Either type

data Either (a : Set ℓ₁) (b : Set ℓ₂) : Set (ℓ₁ ⊔ ℓ₂) where
  Left : a → Either a b
  Right : b → Either a b

fmapEither : ∀ {a b c : Set ℓ} → (b → c) → Either a b → Either a c
fmapEither f (Left x) = Left x
fmapEither f (Right x) = Right (f x)

instance
  FunctorEither : Functor (Either a)
  FunctorEither = record { fmap = fmapEither }

bimapEither : ∀ {a b c d : Set ℓ} → (a → b) → (c → d) → Either a c → Either b d
bimapEither f1 f2 (Left x) = Left (f1 x)
bimapEither f1 f2 (Right x) = Right (f2 x)

instance
  BifunctorEither : ∀ {ℓ} → Bifunctor {ℓ = ℓ} Either
  BifunctorEither = record { bimap = bimapEither }

---------------------------------------------------------------------------------
-- The NonEmpty type

data NonEmpty (a : Set ℓ) : Set ℓ where
  _:|_ : a → List a → NonEmpty a

cons : a → NonEmpty a → NonEmpty a
cons x (y :| ys) = x :| (y ∷ ys)

head : NonEmpty a → a
head (x :| _) = x

tail : NonEmpty a → List a
tail (_ :| xs) = xs

toList : NonEmpty a → List a
toList (x :| xs) = x ∷ xs

fmapNonEmpty : (a → b) → NonEmpty a → NonEmpty b
fmapNonEmpty f (x :| xs) = f x :| map f xs

instance
  FunctorNonEmpty : ∀ {ℓ} → Functor {ℓ = ℓ} NonEmpty
  FunctorNonEmpty = record { fmap = fmapNonEmpty }

---------------------------------------------------------------------------------
-- The TestEquality type class

record TestEquality (f : Set ℓ → Set ℓ') : Set (suc ℓ ⊔ ℓ') where
  field
    testEquality : ∀ {p} {q} → f p → f q → Maybe (p ≡ q)

open TestEquality {{...}} public

testEquality≡ : ∀ {p q r : Set ℓ} → p ≡ q → p ≡ r → Maybe (q ≡ r)
testEquality≡ refl refl = Just refl

instance
  TestEquality≡ : ∀ {p : Set ℓ} → TestEquality (_≡_ p)
  TestEquality≡ = record { testEquality = testEquality≡ }

---------------------------------------------------------------------------------
-- List utilities

unzip : List (a × b) → List a × List b
unzip [] = [] , []
unzip ((fst₁ , snd₁) ∷ xs) with unzip xs
... | fsts , snds = fst₁ ∷ fsts , snd₁ ∷ snds

concatMap : (a → List b) → List a → List b
concatMap f [] = []
concatMap f (x ∷ xs) = f x ++ concatMap f xs

-- | The 'transpose' function transposes the rows and columns of its argument.
-- For example,
--
-- >>> transpose [[1,2,3],[4,5,6]]
-- [[1,4],[2,5],[3,6]]
--
-- If some of the rows are shorter than the following rows, their elements are skipped:
--
-- >>> transpose [[10,11],[20],[],[30,31,32]]
-- [[10,20,30],[11,31],[32]]
{-# TERMINATING #-}
transpose : List (List a) → List (List a)
transpose [] = []
transpose ([] ∷ xss) = transpose xss
transpose {a = a} ((x ∷ xs) ∷ xss) = combine x hds xs tls
  where
    -- We tie the calculations of heads and tails together
    -- to prevent heads from leaking into tails and vice versa.
    -- unzip makes the selector thunk arrangements we need to
    -- ensure everything gets cleaned up properly.
    f : List a → List (a × List a)
    f [] = []
    f (hd ∷ tl) = (hd , tl) ∷ []

    pairlists : List a × List (List a)
    pairlists = unzip (concatMap f xss)

    hds : List a
    hds = fst pairlists

    tls : List (List a)
    tls = snd pairlists

    combine : a → List a → List a → List (List a) → List (List a)
    combine y h ys t = (y ∷ h) ∷ transpose (ys ∷ t)

foldr : (a → b → b) → b → List a → b
foldr f z [] = z
foldr f z (x ∷ xs) = f x (foldr f z xs)
